﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Moves to follow the target transform over time.
/// </summary>
public class FollowTarget : MonoBehaviour 
{
    [SerializeField]
    private float timeToReachTarget = 1f;

    private float minimumDistanceFromTarget;
    private Transform target;
    private float t;
    private Vector3 startPosition;

    /// <summary>
    /// The minimum distance to maintain from the target.
    /// </summary>
    public float MinimumDistanceFromTarget
    {
        get { return minimumDistanceFromTarget; }
        set { minimumDistanceFromTarget = value; }
    }

    /// <summary>
    /// Target transform to follow.
    /// </summary>
    public Transform Target
    {
        get { return target; }
        set
        {
            target = value;
            t = 0f;
            startPosition = transform.position;
        }
    }
	
	void Update () 
	{
        if (target != null)
        {
            // Calculate the minimum distance from target.
            Vector3 targetPosition = target.position; //

            // Track the target position over time.
            t += Time.deltaTime / timeToReachTarget;
            transform.position = Vector3.Lerp(startPosition, targetPosition, t);
        }
	}

}
