﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Controls hand behaviour.
/// </summary>
/// <remarks>Requires an Animator.</remarks>>
public class HandController : MonoBehaviour
{
    [SerializeField]
    private Animator animator;

    [SerializeField]
    private Transform handBaseTransform;

    [SerializeField]
    private Vector3 handOrientationWhenPatting = Vector3.up;

    [SerializeField]
    [Range(0f, 1f)]
    private float minimumTimeToFinishPattingNormalised;

    [SerializeField]
    private float maximumTimeToTransitionToPatting;

    [SerializeField]
    private float maximumTimeToTransitionFromPatting;

    [SerializeField]
    private float maximumTimeToFinishPatting;
    
    private HorsePat pettableHorse;
    private bool isPatting;
    private float normalisedPatTime;
    private Vector3 positionAtPatStart;
    private float normalisedPatTransitionToTime;
    private float normalisedPatTransitionFromTime;
    private bool transitioningIntoPat;
    private bool transitioningOutOfPat;
    
    /// <summary>
    /// Activates the hand. If it's over an interactable it'll interact with that item.
    /// </summary>
    public void Activate()
    {
        if (pettableHorse != null)
        {
            // If there's something to pat, start patting it.
            isPatting = true;
            if (maximumTimeToTransitionToPatting > 0f)
            {
                transitioningIntoPat = true;
            }
            normalisedPatTime = 0f;
            normalisedPatTransitionToTime = 0f;
        }
        else
        {
            // Trigger grasping animation if there's no interact target.
            animator.SetTrigger("Grasp");
        }
    }

    /// <summary>
    /// Releases the hand if it was activated.
    /// </summary>
    public void Release()
    {
        // Figure out if the player held the button long enough to complete the pat.
        if (normalisedPatTime > minimumTimeToFinishPattingNormalised)
        {
            // Tell the horse they received a pat.
            if (isPatting && pettableHorse != null)
            {
                pettableHorse.PatReceived();
            }
        }

        // Stop patting.
        isPatting = false;
        if (maximumTimeToTransitionFromPatting > 0f)
        {
            transitioningOutOfPat = true;
        }
        normalisedPatTransitionFromTime = 0f;

        // Reset transform back to the base position.
        transform.SetPositionAndRotation(handBaseTransform.position, handBaseTransform.rotation);
    }

    // Notify that a game object has entered the player interaction trigger zone.
    public void OnInteractTriggerEnter(Collider other)
    {
        // Check if we've entered the range of a pettable horse.
        HorsePat p = other.GetComponent<HorsePat>();
        if (p != null)
        {
            pettableHorse = p;
        }
    }

    // Notify that a game object has exited the player interaction trigger zone.
    public void OnInteractTriggerExit(Collider other)
    {
        // Check if we've exited the range of a pettable horse.
        HorsePat p = other.GetComponent<HorsePat>();
        if (p != null)
        {
            pettableHorse = null;

            // Stop patting if we're patting.
            if (isPatting)
            {
                Release();
            }
        }
    }

    private void Start()
    {
        Debug.Assert(animator != null, "HandController requires an Animator.");
    }

    private void Update()
    {
        // Stop patting if the horse is no longer offering.
        if (pettableHorse == null || !pettableHorse.IsOfferingPat)
        {
            isPatting = false;
        }

        // Update hand animation patting state.
        animator.SetBool("Patting", isPatting);

        // Update hand animation to reflect if we're within range of a pattable horse and it's offering.
        animator.SetBool("Open", pettableHorse != null && pettableHorse.IsOfferingPat);

        // Handle patting. 
        if (isPatting)
        {
            // Move the hand from the pat start to pat end locations on the horse over time.
            normalisedPatTime += Time.deltaTime / maximumTimeToFinishPatting;
            Vector3 patPosition = SmoothInterpVector3(
                pettableHorse.PatStart.position, pettableHorse.PatEnd.position, normalisedPatTime);

            // Handle transitions entering into and exiting out of the pat.
            /*if (transitioningIntoPat)
            {
                // Smoothly move the hand towards patting over time.
                normalisedPatTransitionToTime += Time.deltaTime / maximumTimeToTransitionToPatting;
                transform.position = SmoothInterpVector3(handBaseTransform.position, patPosition, normalisedPatTransitionToTime);

                // Stop transitioning if it finishes.
                if (normalisedPatTransitionToTime >= 1f)
                {
                    transitioningIntoPat = false;
                }
            }
            else if (transitioningOutOfPat)
            {
                // Smoothly move the hand away from patting over time.
                normalisedPatTransitionFromTime += Time.deltaTime / maximumTimeToTransitionFromPatting;
                transform.position = SmoothInterpVector3(patPosition, handBaseTransform.position, normalisedPatTransitionFromTime);

                // Stop transitioning if it finishes.
                if (normalisedPatTransitionFromTime >= 1f)
                {
                    transitioningIntoPat = false;
                }
            }
            else*/
            {
                // Regular pat.
                transform.position = patPosition;
            }

            // Update rotation to match the pat start and end transforms.
            transform.rotation = pettableHorse.PatStart.rotation * Quaternion.Euler(handOrientationWhenPatting);

            // Stop patting if the pat motion finishes.
            if (normalisedPatTime >= 1f)
            {
                Release();
            }
        }
    }

    // Smoothly interpolate between two vectors over time.
    private Vector3 SmoothInterpVector3(Vector3 from, Vector3 to, float t)
    {
        return new Vector3(
            Mathf.SmoothStep(from.x, to.x, normalisedPatTime),
            Mathf.SmoothStep(from.y, to.y, normalisedPatTime),
            Mathf.SmoothStep(from.z, to.z, normalisedPatTime));
    }

}
