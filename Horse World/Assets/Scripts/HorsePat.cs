﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Handles horse behaviour relating to patting.
/// </summary>
/// <remarks>Requires an Animator. 
/// Communicates with the HorseTalk component if its is available.</remarks>
public class HorsePat : MonoBehaviour, ICanReceivePat
{
    [SerializeField]
    private Animator animator;

    [SerializeField]
    private Transform patStart;

    [SerializeField]
    private Transform patEnd;

    [SerializeField]
    private float delayBetweenPatOffers;

    private HorseTalk horseTalk;
    private bool isOfferingPat;
    private bool isPlayerInRange;
    private float timeLastPatReceived;

    public bool IsOfferingPat
    {
        get { return isOfferingPat; }
    }

    // Returns the starting point for patting.
    public Transform PatStart
    {
        get { return patStart; }
    }

    // Returns the end point for patting.
    public Transform PatEnd
    {
        get { return patEnd; }
    }

    // Handle the player walking into the pat trigger area.
    public void OnPatTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            isOfferingPat = true;
            isPlayerInRange = true;
        }
    }

    // Handle the player leaving the pat trigger area.
    public void OnPatTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            isOfferingPat = false;
            isPlayerInRange = false;
        }
    }

    // Handles a pat having been received.
    public void PatReceived()
    {
        if (horseTalk != null)
        {
            // Respond with dialogue.
            horseTalk.RespondToPat();

            // Stop offering to pat.
            isOfferingPat = false;
            timeLastPatReceived = Time.time;
        }
    }

    private void Start()
    {
        Debug.Assert(animator != null, "HorsePat requires an Animator.");
        horseTalk = GetComponent<HorseTalk>();
    }

    private void Update()
    {
        // Reset the pat offer after a minimum delay if the player is still in range.
        if (Time.time - timeLastPatReceived > delayBetweenPatOffers && isPlayerInRange)
        {
            isOfferingPat = true;
        }

        // Update offering pat animation state.
        animator.SetBool("OfferingPat", isOfferingPat);
    }
}
