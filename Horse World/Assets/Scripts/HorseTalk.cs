﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Manages horse character talking behaviour.
/// </summary>
/// <remarks>Requires RandomVariationSoundSelector components to be able to talk.</remarks>
public class HorseTalk : MonoBehaviour
{
    [Tooltip("Delay in seconds after talking before being able to greet the player again.")]
    [SerializeField]
    private float greetTalkDelay;

    [SerializeField]
    private RandomVariationSoundSelector greetingSounds;

    [SerializeField]
    private RandomVariationSoundSelector patResponseSounds;

    private float timeLastSpoke;
    private bool isGreeting;
    private bool hasSpoken;

    /// <summary>
    /// True if the character is currently talking.
    /// </summary>
    public bool IsTalking
    {
        get { return isGreeting; }
    }
    
    // Handle the player walking into the talk trigger area.
    public void OnTalkTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            // Talk when the player approaches.
            Greet();
        }
    }

    // Handle the player leaving the talk trigger area.
    public void OnTalkTriggerExit(Collider other) {}

    // Handle the talk sound finishing.
    public void OnSoundFinished()
    {
        isGreeting = false;
        hasSpoken = true;
        timeLastSpoke = Time.time;
    }

    // The horse greets the player if enough time has passed since they last spoke.
    public void Greet()
    {
        // Determine if enough time has passed since we last spoke (if we've spoken before).
        bool enoughTimePassedSinceLastSpoke = hasSpoken ? (Time.time - timeLastSpoke > greetTalkDelay) : true;

        // Only talk if we have a sound selector, we're not already talking and enough time has passed.
        if (greetingSounds != null && !isGreeting && enoughTimePassedSinceLastSpoke)
        {
            greetingSounds.Play();
            isGreeting = true;
        }
    }

    // The horse responds to being patted.
    public void RespondToPat()
    {
        // Talk if we have a sound selector.
        if (patResponseSounds != null)
        {
            patResponseSounds.Play();
        }
    }

}
