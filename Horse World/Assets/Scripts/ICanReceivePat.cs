﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The ability to receive pats.
/// </summary>
public interface ICanReceivePat
{
    /// <summary>
    /// Returns true if a pat is being offered.
    /// </summary>
    bool IsOfferingPat { get; }
}
