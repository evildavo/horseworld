﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Handles the movement of an object when given user input.
/// </summary>
public interface IMovementController 
{
    /// <summary>
    /// Applies movement to the object.
    /// </summary>
    /// <param name="horizontalInput">Input along the horizontal axis. Expected to be between -1 and 1.</param>
    /// <param name="verticalInput">Input along the vertical axis. Expected to be between -1 and 1.</param>
    void Move(float horizontalInput, float verticalInput);

}
