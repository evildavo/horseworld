﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Selector for playing audio clips.
/// </summary>
public interface ISoundSelector
{

    /// <summary>
    /// Select a sound clip and play it.
    /// </summary>
    void Play();

}
