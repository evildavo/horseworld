﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Moves constantly at a specified velocity per second.
/// </summary>
/// <remarks>Doesn't use the physics system.</remarks>
public class MoveConstantly : MonoBehaviour
{
    [SerializeField]
    private Vector3 velocity;
    
	void Update ()
    {
        Vector3 position = transform.position;
        position.x += velocity.x * Time.deltaTime;
        position.y += velocity.y * Time.deltaTime;
        position.z += velocity.z * Time.deltaTime;
        transform.position = position;
    }
}
