﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Event handler for trigger events. The parameter is the other collider involved in the trigger collision.
/// </summary>
[System.Serializable]
public class TriggerEvent : UnityEvent<Collider> {}

/// <summary>
/// Passes on the event that this object entered or exited a trigger to the given event handlers. 
/// </summary>
public class NotifyOnTriggerEnterOrExit : MonoBehaviour
{
    [SerializeField]
    private TriggerEvent onTriggerEnter;

    [SerializeField]
    private TriggerEvent onTriggerExit;

    void OnTriggerEnter(Collider other)
    {
        onTriggerEnter.Invoke(other);
    }

    private void OnTriggerExit(Collider other)
    {
        onTriggerExit.Invoke(other);
    }
}
