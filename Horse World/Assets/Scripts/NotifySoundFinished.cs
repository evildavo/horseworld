﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Passes on the event that the audio source stopped playing any sound. 
/// </summary>
[RequireComponent(typeof(AudioSource))]
public class NotifySoundFinished : MonoBehaviour 
{
    [SerializeField]
    private UnityEvent audioFinishedEvent;

    private AudioSource source;
    private bool isPlaying = false;
	
	void Start () 
	{
        source = GetComponent<AudioSource>();
	}
	
	void Update () 
	{
        // Notify the event listener at the first frame the audio source stops playing.
        if (source.isPlaying)
        {
            isPlaying = true;
        }
        else
        {
            if (isPlaying)
            {
                audioFinishedEvent.Invoke();
            }
            isPlaying = false;
        }        
    }
}
