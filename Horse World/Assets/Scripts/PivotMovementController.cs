﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Movement control system where horizontal input turns left and right,
/// and vertical input moves forward and backward.
/// </summary>
[RequireComponent(typeof(Rigidbody))]
public class PivotMovementController : MonoBehaviour, IMovementController
{
    // Speed of movement per second.
    [SerializeField]
    private float speed;

    // Speed of rotation per second.
    [SerializeField]
    private float rotation;

    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    public void Move(float horizontalInput, float verticalInput)
    {
        Vector3 forwardMovement = new Vector3(0f, 0f, verticalInput * speed);
        Vector3 torque = new Vector3(0f, horizontalInput * rotation, 0f);
        
        // Add sudden force, ignoring mass.
        rb.AddForce(rb.rotation * forwardMovement * Time.deltaTime, ForceMode.VelocityChange);

        // Add sudden rotation force, ignoring mass.
        rb.AddTorque(torque, ForceMode.VelocityChange);
    }
}

