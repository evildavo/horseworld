﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Handles player input and passing on the information to a movement controller and hand.
/// </summary>
/// <remarks>Requires an IMovementController component to move.</remarks>
public class PlayerInput : MonoBehaviour 
{
    [SerializeField]
    private HandController hand;

    private IMovementController movement;
    
    void Start () 
	{
        movement = GetComponent<IMovementController>();
	}
	
	void FixedUpdate () 
	{
        // Handle physics-affecting input.
        if (movement != null)
        {
            // Get input.
            float horizontalInput = Input.GetAxis("Horizontal");
            float verticalInput = Input.GetAxis("Vertical");

            // Apply movement to the controller.
            movement.Move(horizontalInput, verticalInput);
        }
	}

    private void Update()
    {
        // Handle hand input.
        if (hand != null)
        {
            // Activate the hand if the fire button is pressed. 
            if (Input.GetButtonDown("Fire1"))
            {
                hand.Activate();
            }

            // Release the hand if the fire button is released.
            else if (Input.GetButtonUp("Fire1"))
            {
                hand.Release();
            }
        }

        // Quit on exit button pressed.
        if (Input.GetButtonDown("Exit"))
        {
            Application.Quit();
        }
    }
}
