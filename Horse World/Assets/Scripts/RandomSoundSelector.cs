﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Allows a random sound to be selected and played from a list of sound clips.
/// </summary>
/// <remarks>Requires an audio source to play sounds.</remarks>
public class RandomSoundSelector : MonoBehaviour, ISoundSelector
{
    [SerializeField]
    private AudioSource source;

    [SerializeField]
    private AudioClip[] sounds;
    	
	public void Play()
    {
        if (source != null)
        {
            // Select a random sound.
            int index = Random.Range(0, sounds.Length - 1);
            source.clip = sounds[index];

            // Play it.
            source.Play();
        }
    }

}
