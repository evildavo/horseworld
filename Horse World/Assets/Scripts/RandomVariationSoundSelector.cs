﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Type of sound, including a list of possible variations on that sound.
/// </summary>
[System.Serializable]
public struct SoundType
{
    public AudioClip[] variations;
}


/// <summary>
/// Allows a random sound to be selected and played from a list of sound types possibly including
/// a number of variation clips.
/// </summary>
/// <remarks>Requires an audio source to play sounds.</remarks>
public class RandomVariationSoundSelector : MonoBehaviour, ISoundSelector
{
    [SerializeField]
    private AudioSource source;

    [SerializeField]
    private SoundType[] sounds;

    public void Play()
    {
        if (source != null && sounds.Length > 0)
        {
            // Choose a random sound type.
            int i = Random.Range(0, sounds.Length);
            Debug.Assert(sounds[i].variations.Length > 0, "Sound [" + i + "] is missing variations.");

            // Select a random variation to play.
            int j = Random.Range(0, sounds[i].variations.Length);
            source.clip = sounds[i].variations[j];

            // Play it.
            source.Play();
        }
    }

}
