﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Movement control system where horizontal input strafes left and right,
/// and vertical input moves forward and backward.
/// </summary>
[RequireComponent(typeof(Rigidbody))]
public class StrafeMovementController : MonoBehaviour, IMovementController
{
    // Speed of movement per second.
    [SerializeField]
    private float speed;

    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    public void Move(float horizontalInput, float verticalInput)
    {
        Vector3 movement = new Vector3(horizontalInput, 0f, verticalInput);

        // Add sudden force, ignoring mass.
        rb.AddForce(rb.rotation * movement * speed * Time.deltaTime, ForceMode.VelocityChange);
    }
}
